@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-dark text-light d-flex justify-content-between">
                    <div>{{ __('Task App') }}</div>
                    <div>{{ auth()->user()->name }}</div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="post" action="/tasks" placeholder="add task" class="mb-3">
                        @csrf
                        <input type="text" name="task" placeholder="Add Task">
                        <input type="hidden" name="status" value="pending">
                        <button type="submit">Submit</button>
                    </form>
                        @if(count($tasks) > 0)
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Task</th>
                                    <th>Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tasks as $task)
                                    <tr>
                                        <td>{{ $task->name }}</td>
                                        <td>{{ $task->status }}</td>
                                        <td class="d-flex justify-content-around">
                                            <a class="btn btn-primary" href="/tasks/{{$task->id}}" type="button">Edit</a>
                                            <form method="post" action="/tasks/{{$task->id}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
