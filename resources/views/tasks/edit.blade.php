@extends('layouts.app')

@section('content')
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>

    <h1>Edit task</h1>
    <a href="/home" class="btn btn-primary" type="button">Go Back</a>
    <form method="post" action="/tasks/{{$task->id}}">
        @csrf
        @method('PUT')
        <div class="form-group">
                <label for="name">Title</label>
                <input class="form-control" placeholder="name" name="name" type="text" id="name" value="{{ $task->name }}">
        </div>

        <div class="form-group">
                <label for="status">Status</label>
                <input class="form-control" placeholder="status" name="status" type="text" id="status" value="{{ $task->status }}">
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Save Changes</button>
    </form>

<script>
ClassicEditor
.create( document.querySelector( '#body' ) )
.catch( error => {
console.error( error );
} );
</script>
@endsection