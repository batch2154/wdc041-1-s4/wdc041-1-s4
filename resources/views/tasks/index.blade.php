@extends('layouts.app')

@section('content')

    @if(count($tasks) > 0)
        @foreach($tasks as $task)
            <tr>
                <td>{{$task->name}}</td>
                <td>{{$task->status}}</td>
            </tr>
        @endforeach
        {{ $tasks->Links() }}
    
    @endif

@endsection